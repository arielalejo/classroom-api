package com.ariel.demo.service;

import com.ariel.demo.dao.IStudentDao;
import com.ariel.demo.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class StudentService {

    private final IStudentDao studentDao;

    @Autowired
    public StudentService(@Qualifier("fakeDao") IStudentDao studentDao){
        this.studentDao = studentDao;
    }

    public Student createStudent(Student student){
        UUID studentId = UUID.randomUUID();
        return this.studentDao.createStudent(studentId, student);
    }

    public Student getStudentById(UUID studentID){
        return this.studentDao.selectStudentById(studentID);
    }

    public List<Student> getAllStudents(){
        return this.studentDao.selectAllStudents();
    }

    public Student updateStudent(UUID studentId, Student student){
        return this.studentDao.updateStudent(studentId, student);
    }

    public UUID deleteStudent(UUID studentId){
        return this.studentDao.deleteStudent(studentId);
    }
}
