package com.ariel.demo.model;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Student {
    @JsonProperty("id") private UUID id;
    @JsonProperty("age") private final Integer age;    
    @JsonProperty("firtName") private final String firstName;
    @JsonProperty("lastName") private final String lastName;
    @JsonProperty("course") private final String course;


    public Student(UUID id, Integer age, String firstName, String lastName, String course) {
        this.id = id;
        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.course = course;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id){
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCourse() {
        return course;
    }
}
