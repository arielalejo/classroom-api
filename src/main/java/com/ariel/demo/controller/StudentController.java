package com.ariel.demo.controller;

import java.util.List;
import java.util.UUID;

import com.ariel.demo.model.Student;
import com.ariel.demo.service.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/students")
public class StudentController {
     
    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService){
        this.studentService = studentService;
    }

    @RequestMapping(method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE )
    public List<Student> getAllStudents(){
        return this.studentService.getAllStudents();
    }

    @RequestMapping(method=RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces= MediaType.APPLICATION_JSON_VALUE)
    public Student createStudent(@RequestBody Student student){
        return this.studentService.createStudent(student);
    }

    @RequestMapping(
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE,
        path = "{studentId}"
    )
    public Student getStudentById(@PathVariable("studentId") UUID studentId){
        return this.studentService.getStudentById(studentId);
    }

    @RequestMapping(method=RequestMethod.PUT,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    path = "{studentId}",
                    produces= MediaType.APPLICATION_JSON_VALUE)
    public Student updateStudent(@PathVariable("studentId") UUID studentId, @RequestBody Student student){
        System.out.print(studentId);
        return this.studentService.updateStudent(studentId, student);
        
    }

    @RequestMapping(
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE,
        path = "{studentId}"
    )
    public UUID deleteStudent(@PathVariable("studentId") UUID studentId){
        return this.studentService.deleteStudent(studentId);
    }

}
