package com.ariel.demo.dao;

import com.ariel.demo.model.Student;
import java.util.List;
import java.util.UUID;

public interface IStudentDao {
    Student createStudent(UUID studentID, Student student);
    Student selectStudentById(UUID studentID);
    List<Student> selectAllStudents();
    Student updateStudent(UUID studentId, Student student);
    UUID deleteStudent(UUID studentId);

}
