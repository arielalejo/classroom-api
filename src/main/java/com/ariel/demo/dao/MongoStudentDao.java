package com.ariel.demo.dao;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.ariel.demo.model.Student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;


@Repository("mongoDbDao")
public class MongoStudentDao implements IStudentDao {

    @Autowired
    private IStudentMongoDbTemplate studentMongoDao;

    private final MongoTemplate mongoTemplate;

    @Autowired
    public MongoStudentDao(MongoTemplate mongoTemplate){
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Student createStudent(UUID studentID, Student student) {
        Student newStudent = studentMongoDao.save(new Student(studentID, student.getAge(), student.getFirstName(), student.getLastName(), student.getCourse()));
        return newStudent;
    }

    @Override
    public Student selectStudentById(UUID studentID) {
        Optional<Student> student = studentMongoDao.findById(studentID);
        return student.get();
    }

    @Override
    public List<Student> selectAllStudents() {
        List<Student> students = studentMongoDao.findAll();
        return students;
    }

    @Override
    public Student updateStudent(UUID studentId, Student student) {
        Optional<Student> studentData = studentMongoDao.findById(studentId);
        if(!studentData.isPresent()) return null;

        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(studentId));
        Update update = new Update();
        update.set("age", student.getAge());
        update.set("firstName", student.getFirstName());
        update.set("lastName", student.getLastName());
        update.set("course", student.getCourse());

        Student oldData = mongoTemplate.findAndModify(query, update, Student.class);
        return oldData;
    }

    @Override
    public UUID deleteStudent(UUID studentId) {
        studentMongoDao.deleteById(studentId);
        return studentId;
    }
    
}
