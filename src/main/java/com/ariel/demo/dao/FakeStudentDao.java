package com.ariel.demo.dao;

import com.ariel.demo.model.Student;
import org.springframework.stereotype.Repository;
import java.util.*;

@Repository("fakeDao")
public class FakeStudentDao implements IStudentDao{

    private final Map<UUID, Student> database;

    public FakeStudentDao(){
        this.database = new HashMap<>();
        UUID id1 = UUID.randomUUID();
        Student student1 = new Student(id1, 30, "ariel", "alejo", "Spring Boot 101");
        this.database.put(id1, student1);
    }

    @Override
    public Student createStudent(UUID studentID, Student student) {
        student.setId(studentID);
        this.database.put(studentID, student);
        return student;
    }

    @Override
    public Student selectStudentById(UUID studentID) {
        return this.database.get(studentID);
    }

    @Override
    public List<Student> selectAllStudents() {
        return new ArrayList<Student>(this.database.values());
    }

    @Override
    public Student updateStudent(UUID studentId, Student student) {
        student.setId(studentId);
        this.database.put(studentId, student);
        return student;
    }

    @Override
    public UUID deleteStudent(UUID studentId) {
        this.database.remove(studentId);
        return studentId;
    }
}
