package com.ariel.demo.dao;

// import java.util.List;

import com.ariel.demo.model.Student;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface IStudentMongoDbTemplate extends MongoRepository<Student, UUID>{
    // public List<Student> findByStudents();
}
