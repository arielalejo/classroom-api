package com.ariel.demo;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class SpringBootCourseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCourseApplication.class, args);		
	}

	@Bean
	ApplicationRunner appRunner(Environment env){
		return args -> System.out.println("DBHOST from application.properties: " + env.getProperty("spring.data.mongodb.host"));
	}


}
