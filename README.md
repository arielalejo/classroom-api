# classroom-api

REST API built in spring boot, with a persistence in MongoDb

**For running the containerized service **

`docker-compose up --build`

The service will be run in the port 8085

You can make a GET request to [localhost:8085/api/v1/students](localhost:8085/api/v1/students)
