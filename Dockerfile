FROM maven:3.6.3-jdk-11
RUN mkdir /usr/app
COPY ./target/demo-0.0.1-SNAPSHOT.jar /usr/app
WORKDIR /usr/app
ENTRYPOINT ["java","-jar","/usr/app/demo-0.0.1-SNAPSHOT.jar"]